
/*
 *  Copyright (C) 1998-2021 by Northwoods Software Corporation. All Rights Reserved.
 */
/*
 * This is an extension and not part of the main GoJS library.
 * Note that the API for this class may change with any version, even point releases.
 * If you intend to use an extension in production, you should copy the code to your own source directory.
 * Extensions can be found in the GoJS kit under the extensions or extensionsTS folders.
 * See the Extensions intro page (https://gojs.net/latest/intro/extensions.html) for more information.
 */
import * as go from 'gojs';

/**
 * A custom LayeredDigraphLayout that knows about "lanes"
 * and that positions each node in its respective lane.

 * This assumes that each Node.data.lane property is a string that names the lane the node should be in.
 * You can set the {@link #laneProperty} property to use a different data property name.
 * It is commonplace to set this property to be the same as the {@link GraphLinksModel#nodeGroupKeyProperty},
 * so that the one property indicates that a particular node data is a member of a particular group
 * and thus that that group represents a lane.

 * The lanes can be sorted by specifying the {@link #laneComparer} function.

 * You can add extra space between the lanes by increasing {@link #laneSpacing} from its default of zero.
 * That number's unit is columns, {@link LayeredDigraphLayout#columnSpacing}, not in document coordinates.
 * @category Layout Extension
 */
export class BPMNSwimLaneLayout extends go.LayeredDigraphLayout {
	constructor() {
		super({ ...arguments, alignOption: go.LayeredDigraphLayout.AlignAll });
		// settable properties
		this._laneProperty = 'group'; // how to get lane identifier string from node data
		this._laneNames = []; // lane names, may be sorted using this.laneComparer
		this._laneComparer = null;
		this._laneSpacing = 0; // in columns
		this._router = { linkSpacing: 4 };
		this._reducer = null;
		// computed, read-only state
		this._lanePositions = new go.Map(); // lane names --> start columns, left to right
		this._laneBreadths = new go.Map(); // lane names --> needed width in columns
		// internal state
		this._layers = null;
		this._neededSpaces = null;
		this._phases = [];
		this._phaseMembers = {};

		// this._alignOption = go.LayeredDigraphLayout.AlignAll;
		// this._setsPortSpots = true;
		// this.alignOption = go.LayeredDigraphLayout.AlignUpperLeft;
	}

	/**
	 * Gets or sets the name of the data property that holds the string which is the name of the lane that the node should be in.
	 * The default value is "lane".
	 */
	get laneProperty() {
		return this._laneProperty;
	}

	set laneProperty(val) {
		if (typeof val !== 'string' && typeof val !== 'function')
			throw new Error(`new value for BPMNSwimLaneLayout.laneProperty must be a property name, not: ${val}`);
		if (this._laneProperty !== val) {
			this._laneProperty = val;
			this.invalidateLayout();
		}
	}

	/**
	 * Gets or sets an Array of lane names.
	 * If you set this before a layout happens, it will use those lanes in that order.
	 * Any additional lane names that it discovers will be added to the end of this Array.
	 *
	 * This property is reset to an empty Array at the end of each layout.
	 * The default value is an empty Array.
	 */
	get laneNames() {
		return this._laneNames;
	}

	set laneNames(val) {
		if (!Array.isArray(val))
			throw new Error(`new value for BPMNSwimLaneLayout.laneNames must be an Array, not: ${val}`);
		if (this._laneNames !== val) {
			this._laneNames = val;
			this.invalidateLayout();
		}
	}

	/**
	 * Gets or sets a function by which to compare lane names, for ordering the lanes within the {@link #laneNames} Array.
	 * By default the function is null -- the lanes are not sorted.
	 */
	get laneComparer() {
		return this._laneComparer;
	}

	set laneComparer(val) {
		if (typeof val !== 'function')
			throw new Error(`new value for BPMNSwimLaneLayout.laneComparer must be a function, not: ${val}`);
		if (this._laneComparer !== val) {
			this._laneComparer = val;
			this.invalidateLayout();
		}
	}

	/**
	 * Gets or sets the amount of additional space it allocates between the lanes.
	 * This number specifies the number of columns, with the same meaning as {@link LayeredDigraphLayout#columnSpacing}.
	 * The number unit is not in document coordinate or pixels.
	 * The default value is zero columns.
	 */
	get laneSpacing() {
		return this._laneSpacing;
	}

	set laneSpacing(val) {
		if (typeof val !== 'number')
			throw new Error(`new value for BPMNSwimLaneLayout.laneSpacing must be a number, not: ${val}`);
		if (this._laneSpacing !== val) {
			this._laneSpacing = val;
			this.invalidateLayout();
		}
	}

	/**
	 * @hidden
	 */
	get router() {
		return this._router;
	}

	set router(val) {
		if (this._router !== val) {
			this._router = val;
			this.invalidateLayout();
		}
	}

	/**
	 * @hidden
	 */
	get reducer() {
		return this._reducer;
	}

	set reducer(val) {
		if (this._reducer !== val) {
			this._reducer = val;

			if (val) {
				const lay = this;
				val.findLane = function (v) {
					return lay.getLane(v);
				};
				val.getIndex = function (v) {
					return v.index;
				};
				val.getBary = function (v) {
					return v.bary || 0;
				};
				val.setBary = function (v, val) {
					v.bary = val;
				};
				val.getConnectedNodesIterator = function (v) {
					return v.vertexes;
				};
			}

			this.invalidateLayout();
		}
	}

	/**
	 * The computed positions of each lane,
	 * in the form of a {@link Map} mapping lane names (strings) to numbers.
	 */
	get lanePositions() {
		return this._lanePositions;
	}

	/**
	 * The computed breadths (widths or heights depending on the direction) of each lane,
	 * in the form of a {@link Map} mapping lane names (strings) to numbers.
	 */
	get laneBreadths() {
		return this._laneBreadths;
	}

	/**
	 * @hidden
	 * @param coll
	 */
	doLayout(coll) {
		this.lanePositions.clear(); // lane names --> start columns, left to right
		this.laneBreadths.clear(); // lane names --> needed width in columns
		this._layers = null;
		this._neededSpaces = null;
		super.doLayout(coll);
		this.computePhases();
		this.lanePositions.clear();
		this.laneBreadths.clear();
		this._layers = null;
		this._neededSpaces = null;
		this.laneNames = []; // clear out for next layout
	}

	/**
	 * @hidden
	 * @param v
	 * @param topleft
	 */
	nodeMinLayerSpace(v, topleft) {
		if (!this._neededSpaces) this._neededSpaces = this.computeNeededLayerSpaces(this.network);
		if (v.node === null) return 0;
		let lay = v.layer;
		if (!topleft) {
			if (lay > 0) lay -= 1;
		}
		const overlaps = (this._neededSpaces[lay] || 0) / 2;
		const edges = this.countEdgesForDirection(v, this.direction > 135 ? !topleft : topleft);
		const needed = Math.max(overlaps, edges) * this.router.linkSpacing * 1.5;
		if (this.direction === 90 || this.direction === 270) {
			if (topleft) {
				return v.focus.y + 10 + needed;
			} else {
				return v.bounds.height - v.focus.y + 10 + needed;
			}
		} else {
			if (topleft) {
				return v.focus.x + 10 + needed;
			} else {
				return v.bounds.width - v.focus.x + 10 + needed;
			}
		}
	}

	countEdgesForDirection(vertex, topleft) {
		let c = 0;
		const lay = vertex.layer;
		vertex.edges.each((e) => {
			if (topleft) {
				if (e.getOtherVertex(vertex).layer >= lay) c++;
			} else {
				if (e.getOtherVertex(vertex).layer <= lay) c++;
			}
		});
		return c;
	}

	computeNeededLayerSpaces(net) {
		// group all edges by their connected vertexes' least layer
		const layerMinEdges = [];
		net.edges.each((e) => {
			// consider all edges, including dummy ones!
			const f = e.fromVertex;
			const t = e.toVertex;
			if (f.column === t.column) return; // skip edges that don't go between columns
			if (Math.abs(f.layer - t.layer) > 1) return; // skip edges that don't go between adjacent layers
			const lay = Math.min(f.layer, t.layer);
			let arr = layerMinEdges[lay];
			if (!arr) arr = layerMinEdges[lay] = [];
			arr.push(e);
		});
		// sort each array of edges by their lowest connected vertex column
		// for edges with the same minimum column, sort by their maximum column
		const layerMaxEdges = []; // same as layerMinEdges, but sorted by maximum column
		layerMinEdges.forEach((arr, lay) => {
			if (!arr) return;
			arr.sort((e1, e2) => {
				const f1c = e1.fromVertex.column;
				const t1c = e1.toVertex.column;
				const f2c = e2.fromVertex.column;
				const t2c = e2.toVertex.column;
				const e1mincol = Math.min(f1c, t1c);
				const e2mincol = Math.min(f2c, t2c);
				if (e1mincol > e2mincol) return 1;
				if (e1mincol < e2mincol) return -1;
				const e1maxcol = Math.max(f1c, t1c);
				const e2maxcol = Math.max(f2c, t2c);
				if (e1maxcol > e2maxcol) return 1;
				if (e1maxcol < e2maxcol) return -1;
				return 0;
			});
			layerMaxEdges[lay] = arr.slice(0);
			layerMaxEdges[lay].sort((e1, e2) => {
				const f1c = e1.fromVertex.column;
				const t1c = e1.toVertex.column;
				const f2c = e2.fromVertex.column;
				const t2c = e2.toVertex.column;
				const e1maxcol = Math.max(f1c, t1c);
				const e2maxcol = Math.max(f2c, t2c);
				if (e1maxcol > e2maxcol) return 1;
				if (e1maxcol < e2maxcol) return -1;
				const e1mincol = Math.min(f1c, t1c);
				const e2mincol = Math.min(f2c, t2c);
				if (e1mincol > e2mincol) return 1;
				if (e1mincol < e2mincol) return -1;
				return 0;
			});
		});

		// run through each array of edges to count how many overlaps there might be
		const layerOverlaps = [];
		layerMinEdges.forEach((arr, lay) => {
			const mins = arr; // sorted by min column
			const maxs = layerMaxEdges[lay]; // sorted by max column
			let maxoverlap = 0; // maximum count for this layer
			if (mins && maxs && mins.length > 1 && maxs.length > 1) {
				let mini = 0;
				let min = null;
				let maxi = 0;
				let max = null;
				while (mini < mins.length || maxi < maxs.length) {
					if (mini < mins.length) min = mins[mini];
					const mincol = min ? Math.min(min.fromVertex.column, min.toVertex.column) : 0;
					if (maxi < maxs.length) max = maxs[maxi];
					const maxcol = max ? Math.max(max.fromVertex.column, max.toVertex.column) : Infinity;
					maxoverlap = Math.max(maxoverlap, Math.abs(mini - maxi));
					if (mincol <= maxcol && mini < mins.length) {
						mini++;
					} else if (maxi < maxs.length) {
						maxi++;
					}
				}
			}
			layerOverlaps[lay] = maxoverlap * 1.5; // # of parallel links
		});
		return layerOverlaps;
	}

	assignLayers() {
		super.assignLayers();
		var it = this.network.vertexes.iterator;
		while (it.next()) {
			var v = it.value;
			if (v.node !== null) {
				var lay = v.node.data.level;
				// console.log('lay', lay)
				if (typeof lay === 'number' && lay >= 0) {
					v.layer = lay;
				}
			}
		}
	}

	setupLanes() {
		// set up some data structures
		const layout = this;
		const laneNameSet = new go.Set().addAll(this.laneNames);
		const laneIndexes = new go.Map(); // lane names --> index when sorted
		const layers = [];
		this._layers = layers;

		const vit = this.network.vertexes.iterator;
		while (vit.next()) {
			const v = vit.value;
			const lane = this.getLane(v); // cannot call findLane yet
			if (lane !== null && !laneNameSet.has(lane)) {
				laneNameSet.add(lane);
				this.laneNames.push(lane);
			}
			const { layer } = v;
			if (layer >= 0) {
				const arr = layers[layer];
				if (!arr) {
					layers[layer] = [v];
				} else {
					arr.push(v);
				}
			}
		}

		// sort laneNames and initialize laneIndexes
		if (typeof this.laneComparer === 'function') this.laneNames.sort(this.laneComparer);
		for (let i = 0; i < this.laneNames.length; i++) {
			laneIndexes.add(this.laneNames[i], i);
		}
		// now OK to call findLane

		// sort vertexes so that vertexes are grouped by lane
		for (let i = 0; i <= this.maxLayer; i++) {
			this._layers[i].sort((a, b) => layout.compareVertexes(a, b));
		}
	}

	setupPhases() {
		const vit = this.network.vertexes.iterator;
		while (vit.next()) {
			const v = vit.value;
			const { node } = v;
			if (!node) return;
			const { data } = node;
			if (data.category === 'PhaseGroup') {
				this._phases.push(data.key);
			} else if (data.phase !== undefined) {
				if (!this._phaseMembers[data.phase]) {
					this._phaseMembers[data.phase] = [];
				}
				this._phaseMembers[data.phase].push(this.diagram.findNodeForKey(data.key));
			}
		}
	}

	/**
	 * @hidden
	 * Replace the standard reduceCrossings behavior so that it respects lanes.
	 */
	reduceCrossings() {
		this.setupLanes();
		this.setupPhases();

		// this just cares about the .index and ignores .column
		const layers = this._layers;
		const red = this.reducer;
		if (red) {
			for (let i = 0; i < layers.length - 1; i++) {
				red.reduceCrossings(layers[i], layers[i + 1]);
				layers[i].forEach((v, j) => {
					v.index = j;
				});
			}
			for (let i = layers.length - 1; i > 0; i--) {
				red.reduceCrossings(layers[i], layers[i - 1]);
				layers[i].forEach((v, j) => {
					v.index = j;
				});
			}
		}

		this.computeLanes(); // and recompute all vertex.column values
		this.computePhases();
	}

	computePhases() {
		let totalLaneHeight = 0;
		for (let i = 0; i < this.laneNames.length; i += 1) {
			const lane = this.laneNames[i];
			totalLaneHeight += this.laneBreadths.get(lane) * this.columnSpacing;
		}

		const phaseBound = this._phases
			.map((phase) => ({
				phase,
				bound: this.diagram.computePartsBounds(this._phaseMembers[phase]),
			}))
			.sort((p1, p2) => {
				if (p1.bound.x > p2.bound.x) return 1;
				return -1;
			});

		if (phaseBound.length === 0) return;

		phaseBound[0].bound.position = new go.Point(
			phaseBound[0].bound.position.x - 21,
			phaseBound[0].bound.position.y
		);
		phaseBound[0].bound.size = new go.Size(phaseBound[0].bound.size.width + 21, phaseBound[0].bound.size.height);

		phaseBound.forEach(({ phase, bound }, index) => {
			const phaseNode = this.diagram.findNodeForKey(phase);
			phaseNode.location = new go.Point(bound.position.x, -26);
			const body = phaseNode.findObject('BODY');
			let { width } = bound.size;
			if (
				index + 1 < phaseBound.length &&
				phaseBound[index + 1].bound.position.x > bound.position.x + bound.size.width
			) {
				const delta = (phaseBound[index + 1].bound.position.x - bound.position.x - bound.size.width) / 2;
				width += delta;

				phaseBound[index + 1].bound.position = new go.Point(
					phaseBound[index + 1].bound.position.x - delta,
					phaseBound[index + 1].bound.position.y
				);
				phaseBound[index + 1].bound.size = new go.Size(
					phaseBound[index + 1].bound.size.width + delta,
					phaseBound[index + 1].bound.size.height
				);
			}
			if (body) body.desiredSize = new go.Size(width, totalLaneHeight);
		});
	}

	computeLanes() {
		// compute needed width for each lane, in columns
		for (let i = 0; i < this.laneNames.length; i++) {
			const lane = this.laneNames[i];
			this.laneBreadths.add(lane, this.computeMinLaneWidth(lane)); // set default width of lane to the header width, in columns
		}
		const lwidths = new go.Map(); // reused for each layer
		for (let i = 0; i <= this.maxLayer; i++) {
			const arr = this._layers[i];
			if (arr) {
				const layout = this;
				// now run through Array finding width (in columns) of each lane
				// and max with this.laneBreadths.get(lane)
				for (let j = 0; j < arr.length; j++) {
					const v = arr[j];
					const w = this.nodeMinColumnSpace(v, true) + 1 + this.nodeMinColumnSpace(v, false);
					const ln = this.findLane(v) || '';
					const totw = lwidths.get(ln);
					if (totw === null) {
						lwidths.set(ln, w);
						// console.log('lane ', ln, ' width: ', w, v.node.data.key);
					} else {
						lwidths.set(ln, totw + w);
						// console.log('lane ', ln, ' totw width: ', totw + w, v.node.data.key);
					}
				}
				lwidths.each((kvp) => {
					const lane = kvp.key;
					const colsInLayer = kvp.value;
					const colsMax = layout.laneBreadths.get(lane) || 0;
					if (colsInLayer > colsMax) layout.laneBreadths.set(lane, colsInLayer);
				});
				lwidths.clear();
			}
		}

		// add lane padding
		// for (let i = 0; i < this.laneNames.length; i++) {
		// 	const lane = this.laneNames[i];
		// 	const w = this.laneBreadths.get(lane);
		// 	this.laneBreadths.set(lane, w + 2.5);
		// }

		// compute starting positions for each lane
		let x = 0;
		for (let i = 0; i < this.laneNames.length; i++) {
			const lane = this.laneNames[i];
			this.lanePositions.set(lane, x);
			const w = this.laneBreadths.get(lane) || 0;
			x += w + this.laneSpacing;
		}
		this.renormalizeColumns();
	}

	renormalizeColumns() {
		// set new column and index on each vertex
		for (let i = 0; i < this._layers.length; i++) {
			let prevlane = null;
			let c = 0;
			const arr = this._layers[i];
			for (let j = 0; j < arr.length; j++) {
				const v = arr[j];
				v.index = j;
				const l = this.findLane(v);
				if (l && prevlane !== l) {
					c = this.lanePositions.get(l) || 0;
					const w = this.laneBreadths.get(l) || 0;
					// compute needed breadth within lane, in columns
					let z = this.nodeMinColumnSpace(v, true) + 1 + this.nodeMinColumnSpace(v, false);
					let k = j + 1;
					while (k < arr.length && this.findLane(arr[k]) === l) {
						const vz = arr[k];
						z += this.nodeMinColumnSpace(vz, true) + 1 + this.nodeMinColumnSpace(vz, false);
						k++;
					}
					// if there is extra space, shift the vertexes to the middle of the lane
					if (z < w) {
						c += Math.floor((w - z) / 2);
					}
				}
				c += this.nodeMinColumnSpace(v, true);
				c += 1;
				v.column = c;
				c += this.nodeMinColumnSpace(v, false);
				prevlane = l;
			}
		}
	}

	/**
	 * Return the minimum lane width, in columns
	 * @param lane
	 */
	computeMinLaneWidth(lane) {
		return 0;
	}

	/**
	 * @hidden
	 * Disable normal straightenAndPack behavior, which would mess up the columns.
	 */
	straightenAndPack() {}

	/**
	 * Given a vertex, get the lane (name) that its node belongs in.
	 * If the lane appears to be undefined, this returns the empty string.
	 * For dummy vertexes (with no node) this will return null.
	 * @param v
	 */
	getLane(v) {
		if (v === null) return null;
		const { node } = v;
		if (node !== null) {
			const { data } = node;
			if (data !== null && data.category !== 'PhaseGroup') {
				let lane = null;
				if (typeof this.laneProperty === 'function') {
					lane = this.laneProperty(data);
				} else {
					lane = data[this.laneProperty];
				}
				if (typeof lane === 'string') return lane;
				return ''; // default lane
			}
		}
		return null;
	}

	/**
	 * This is just like {@link #getLane} but handles dummy vertexes
	 * for which the {@link #getLane} returns null by returning the
	 * lane of the edge's source or destination vertex.
	 * This can only be called after the lanes have been set up internally.
	 * @param v
	 */
	findLane(v) {
		if (v !== null) {
			const lane = this.getLane(v);
			if (lane !== null) {
				return lane;
			}

			const srcv = this.findRealSource(v.sourceEdges.first());
			const dstv = this.findRealDestination(v.destinationEdges.first());
			const srcLane = this.getLane(srcv);
			const dstLane = this.getLane(dstv);
			if (srcLane !== null || dstLane !== null) {
				if (srcLane === dstLane) return srcLane;
				if (srcLane !== null) return srcLane;
				if (dstLane !== null) return dstLane;
			}
		}
		return null;
	}

	findRealSource(e) {
		if (e === null) return null;
		const fv = e.fromVertex;
		if (fv && fv.node) return fv;
		return this.findRealSource(fv.sourceEdges.first());
	}

	findRealDestination(e) {
		if (e === null) return null;
		const tv = e.toVertex;
		if (tv.node) return tv;
		return this.findRealDestination(tv.destinationEdges.first());
	}

	compareVertexes(v, w) {
		let laneV = this.findLane(v);
		if (laneV === null) laneV = '';
		let laneW = this.findLane(w);
		if (laneW === null) laneW = '';
		if (laneV < laneW) return -1;
		if (laneV > laneW) return 1;
		if (v.column < w.column) return -1;
		if (v.column > w.column) return 1;
		return 0;
	}

	commitLinks() {
		super.commitLinks();

		const nodeLayerMap = {};
		const nodeVertexMap = {};
		const nodeIndexInLaneMap = {};
		const laneHasLayerMap = {};
		for (let i = 0; i <= this.maxLayer; i++) {
			const arr = this._layers[i];
			// console.log('-----')
			if (arr) {
				for (let j = 0; j < arr.length; j++) {
					const { node } = arr[j];
					if (node) {
						// console.log('node', node.data)
						nodeLayerMap[node.data.key] = i;
						nodeVertexMap[node.data.key] = arr[j];
						const lane = this.findLane(arr[j]);

						laneHasLayerMap[lane] = laneHasLayerMap[lane] || {};
						if (!laneHasLayerMap[lane][i]) {
							laneHasLayerMap[lane][i] = 1;
							let l = i - 1;
							while (l >= 0) {
								if (laneHasLayerMap[lane][l]) {
									laneHasLayerMap[lane][i] = laneHasLayerMap[lane][l] + 1;
									break;
								}
								l -= 1;
							}
						}
						nodeIndexInLaneMap[node.data.key] = laneHasLayerMap[lane][i];
					}
				}
			}
		}

		const isSameRow = (node1, node2) => {
			const { y, height } = nodeVertexMap[node1.data.key];
			const { centerY } = nodeVertexMap[node2.data.key];
			return centerY < y + height && centerY > y;
		};

		const isRowBellow = (node1, node2) => {
			const { y, height } = nodeVertexMap[node1.data.key];
			const { centerY } = nodeVertexMap[node2.data.key];
			return centerY > y + height;
		};

		const isSameColumn = (node1, node2) => {
			const { x, width } = nodeVertexMap[node1.data.key];
			const { centerX } = nodeVertexMap[node2.data.key];
			return centerX > x - width && centerX < x;
		};

		const isLeftColumn = (node1, node2) => {
			const { x } = nodeVertexMap[node1.data.key];
			const { centerX } = nodeVertexMap[node2.data.key];
			return centerX > x;
		};

		// direct same row
		const haveNodeBetweenDirectSameLine = (node1, node2, isDirectToTheLeftSide) => {
			// filter nodes are between node 1 and node 2
			const listNodeBetween = Object.keys(nodeLayerMap).filter((l) => {
				if (isDirectToTheLeftSide) {
					return (
						nodeLayerMap[l] > nodeLayerMap[node1.data.key] && nodeLayerMap[l] < nodeLayerMap[node2.data.key]
					);
				} else {
					return (
						nodeLayerMap[l] > nodeLayerMap[node2.data.key] && nodeLayerMap[l] < nodeLayerMap[node1.data.key]
					);
				}
			});
			const { height, y } = nodeVertexMap[node1.data.key];
			return listNodeBetween.some((n) => {
				const { height: nHeight, y: nY } = nodeVertexMap[n];
				return (nY >= y && nY <= y + height) || (nY + nHeight >= y && nY + nHeight <= y + height);
			});
		};

		// direct same column
		const haveNodeBetweenDirectSameLayer = (node1, node2, isDirectToTheBottomSide) => {
			const listNodeBetween = Object.keys(nodeLayerMap).filter(
				(l) => nodeLayerMap[l] === nodeLayerMap[node1.data.key]
			);
			return listNodeBetween.some((n) => {
				if (isDirectToTheBottomSide) {
					return (
						nodeVertexMap[n].column > nodeVertexMap[node1.data.key].column &&
						nodeVertexMap[n].column < nodeVertexMap[node2.data.key].column
					);
				} else {
					return (
						nodeVertexMap[n].column < nodeVertexMap[node1.data.key].column &&
						nodeVertexMap[n].column > nodeVertexMap[node2.data.key].column
					);
				}
			});
		};

		// check bottom side available
		const isBottomSideAvailable = (node1, node2) => {
			const { y: y1 } = nodeVertexMap[node1.data.key];
			const { height: h2, y: y2 } = nodeVertexMap[node2.data.key];

			return (
				Object.keys(nodeLayerMap).filter((k) => {
					const { height: nHeight, y: nY } = nodeVertexMap[k];
					return nodeLayerMap[k] === nodeLayerMap[node1.data.key] && nY > y1 && nY < y2 + h2;
				}).length === 0
			);
		};

		// check top side available
		const isTopSideAvailable = (node1, node2) => {
			const { height: h1, y: y1 } = nodeVertexMap[node1.data.key];
			// console.log('h1, y: y1', h1, y1)
			const { y: y2 } = nodeVertexMap[node2.data.key];
			// console.log('y2', y2)

			return (
				Object.keys(nodeLayerMap).filter((k) => {
					const { height: nHeight, y: nY } = nodeVertexMap[k];
					// if (node1.data.text === 'st5' && node2.data.text === 'st7' && nodeLayerMap[k] === nodeLayerMap[node1.data.key]) {
					// 	console.log('k', k)
					// 	console.log('+++', nY, nHeight, y2, y1)
					// 	console.log('===', nY + nHeight > y2, nY < y1)
					// }
					return nodeLayerMap[k] === nodeLayerMap[node1.data.key] && nY + nHeight > y2 && nY < y1;
				}).length === 0
			);
		};

		// check right side available
		const isRightSideAvailable = (node1, node2) => {
			const { height, y, centerY } = nodeVertexMap[node1.data.key];

			return (
				Object.keys(nodeLayerMap).filter((k) => {
					const { height: nHeight, y: nY } = nodeVertexMap[k];
					return (
						nodeLayerMap[k] > nodeLayerMap[node2.data.key] &&
						nodeLayerMap[k] < nodeLayerMap[node1.data.key] &&
						centerY <= nY + nHeight &&
						centerY >= nY
						// ((nY >= y && nY <= y + height) || (nY + nHeight >= y && nY + nHeight <= y + height))
					);
				}).length === 0
			);
		};

		// check left side available
		const isLeftSideAvailable = (node1, node2) => {
			const { height, y, centerY } = nodeVertexMap[node1.data.key];

			return (
				Object.keys(nodeLayerMap).filter((k) => {
					const { height: nHeight, y: nY } = nodeVertexMap[k];
					return (
						nodeLayerMap[k] > nodeLayerMap[node1.data.key] &&
						nodeLayerMap[k] < nodeLayerMap[node2.data.key] &&
						centerY <= nY + nHeight &&
						centerY >= nY
						// ((nY >= y && nY <= y + height) || (nY + nHeight >= y && nY + nHeight <= y + height))
					);
				}).length === 0
			);
		};

		const turningDecisionNodeLink = (node) => {
			let canGoUp = true;
			let canGoDown = true;
			let canGoRight = true;
			const it = node.findLinksOutOf();
			let linkArray = [];
			while (it.next()) {
				linkArray.push(it.value);
			}
			// sort by layer left to right
			linkArray = linkArray.sort((l1, l2) => (nodeLayerMap[l1.data.key] < nodeLayerMap[l2.data.key] ? 1 : -1));
			const fromLane = node.data.group;
			const fromLanePos = this.lanePositions.get(fromLane);
			linkArray.forEach((link) => {
				const toLane = link.data.group;
				const toLanePos = this.lanePositions.get(toLane);
				// console.log('link.data', link.data)
				// console.log('node.data', node.data)
				if (fromLanePos > toLanePos) {
					// go up
					if (canGoUp) {
						link.fromSpot = go.Spot.TopSide;
						canGoUp = false;
					} else {
						link.fromSpot = go.Spot.RightSide;
						// link.toSpot = go.Spot.BottomSide
						canGoRight = false;
					}
					if (nodeLayerMap[node.data.key] - nodeLayerMap[link.data.key] === 1) {
						link.toSpot = go.Spot.LeftSide;
					} else {
						link.toSpot = go.Spot.BottomSide;
					}
				} else if (fromLanePos < toLanePos) {
					// go down
					if (canGoDown) {
						link.fromSpot = go.Spot.BottomSide;
						// link.toSpot = go.Spot.LeftSide
						canGoDown = false;
					} else {
						link.fromSpot = go.Spot.RightSide;
						// link.toSpot = go.Spot.LeftSide
						canGoRight = false;
					}
					if (nodeLayerMap[node.data.key] - nodeLayerMap[link.data.key] === 1) {
						link.toSpot = go.Spot.LeftSide;
					} else {
						link.toSpot = go.Spot.TopSide;
					}
				} else {
					// same lane
					if (isSameRow(node, link)) {
						if (nodeIndexInLaneMap[node.data.key] - nodeIndexInLaneMap[link.data.key] === 1) {
							link.fromSpot = go.Spot.RightSide;
							canGoRight = false;
						} else {
							link.fromSpot = go.Spot.BottomSide;
							link.toSpot = go.Spot.BottomSide;
							link.fromEndSegmentLength = 40; // reserve space for link label
							canGoDown = false;
						}
					} else if (isRowBellow(node, link)) {
						link.fromSpot = go.Spot.BottomSide;
						link.toSpot = go.Spot.LeftSide;
						canGoDown = false;
					} else {
						link.fromSpot = go.Spot.TopSide;
						link.toSpot = go.Spot.LeftSide;
						canGoRight = false;
					}
				}
			});
		};

		const turningSameSideOutLink = () => {
			for (let i = 0; i <= this.maxLayer; i++) {
				const arr = this._layers[i];
				if (arr) {
					for (let j = 0; j < arr.length; j++) {
						const { node } = arr[j];
						if (!node) continue;
						const ite = node.findLinksOutOf();
						const ls = [];
						while (ite.next()) {
							if (ite.value.fromSpot === go.Spot.TopSide) {
								ls.push(ite.value);
							}
						}

						if (ls.length > 1 && nodeLayerMap[ls[0].data.key] === nodeLayerMap[ls[1].data.key]) {
							// to nodes in the same layers
							// find the obstacle node
							const nodesInLayer = this._layers[nodeLayerMap[node.data.key]];
							for (let k = 0; k < nodesInLayer.length; k++) {
								const n = nodesInLayer[k].node;
								if (!n) continue;
								if (
									n.position.y < node.position.y &&
									n.position.y >= ls[0].position.y &&
									n.position.y >= ls[1].position.y
								) {
									if (ls[0].position.y > ls[1].position.y) {
										ls[0].toSpot = go.Spot.BottomSide;
									} else {
										ls[1].toSpot = go.Spot.BottomSide;
									}
									break;
								}
							}
						}
					}
				}
			}
		};

		const it = this.network.edges.iterator;
		while (it.next()) {
			const e = it.value;
			const { fromNode, toNode } = e.link;
			const fromLane = fromNode.data.group;
			const toLane = toNode.data.group;
			const fromLanePos = this.lanePositions.get(fromLane);
			const toLanePos = this.lanePositions.get(toLane);

			// if (fromNode.data.category === 'gateway') {
			// 	turningDecisionNodeLink(fromNode);
			// 	continue;
			// }

			if (isSameRow(fromNode, toNode)) {
				// in the same line
				if (nodeLayerMap[fromNode.data.key] < nodeLayerMap[toNode.data.key]) {
					// case direct to the left
					// if (haveNodeBetweenDirectSameLine(fromNode, toNode, true)) {
					if (isLeftSideAvailable(fromNode, toNode)) {
						e.link.fromSpot = go.Spot.LeftSide;
						e.link.toSpot = go.Spot.RightSide;
					} else {
						e.link.fromSpot = go.Spot.BottomSide;
						e.link.toSpot = go.Spot.BottomSide;
					}
				} else {
					// case direct to the right
					// if (haveNodeBetweenDirectSameLine(fromNode, toNode, false)) {
					if (isRightSideAvailable(fromNode, toNode)) {
						e.link.fromSpot = go.Spot.RightSide;
						e.link.toSpot = go.Spot.LeftSide;
					} else {
						e.link.fromSpot = go.Spot.TopSide;
						e.link.toSpot = go.Spot.TopSide;
					}
				}
			} else if (isRowBellow(fromNode, toNode)) {
				// case bellow
				if (nodeLayerMap[fromNode.data.key] === nodeLayerMap[toNode.data.key]) {
					// case same column
					// if (haveNodeBetweenDirectSameLayer(fromNode, toNode, true)) {
					if (isBottomSideAvailable(fromNode, toNode)) {
						e.link.fromSpot = go.Spot.BottomSide;
						e.link.toSpot = go.Spot.TopSide;
					}
					e.link.fromSpot = go.Spot.RightSide;
					e.link.toSpot = go.Spot.RightSide;
				} else if (nodeLayerMap[fromNode.data.key] > nodeLayerMap[toNode.data.key]) {
					// case bottom right column
					if (isBottomSideAvailable(fromNode, toNode) && isLeftSideAvailable(toNode, fromNode)) {
						e.link.fromSpot = go.Spot.BottomSide;
						e.link.toSpot = go.Spot.LeftSide;
					} else if (isRightSideAvailable(fromNode, toNode) && isTopSideAvailable(toNode, fromNode)) {
						e.link.fromSpot = go.Spot.RightSide;
						e.link.toSpot = go.Spot.TopSide;
					} else {
						e.link.fromSpot = go.Spot.BottomSide;
						e.link.toSpot = go.Spot.TopSide;
					}
				} else {
					// case bottom left column
					if (isBottomSideAvailable(fromNode, toNode) && isRightSideAvailable(toNode, fromNode)) {
						e.link.fromSpot = go.Spot.BottomSide;
						e.link.toSpot = go.Spot.RightSide;
					} else if (isLeftSideAvailable(fromNode, toNode) && isTopSideAvailable(toNode, fromNode)) {
						e.link.fromSpot = go.Spot.LeftSide;
						e.link.toSpot = go.Spot.TopSide;
					} else {
						e.link.fromSpot = go.Spot.BottomSide;
						e.link.toSpot = go.Spot.TopSide;
					}
				}
			} else {
				// case above
				if (nodeLayerMap[fromNode.data.key] === nodeLayerMap[toNode.data.key]) {
					// case same column
					// if (haveNodeBetweenDirectSameLayer(fromNode, toNode, true)) {
					if (isTopSideAvailable(fromNode, toNode)) {
						e.link.fromSpot = go.Spot.TopSide;
						e.link.toSpot = go.Spot.BottomSide;
					}
					e.link.fromSpot = go.Spot.LeftSide;
					e.link.toSpot = go.Spot.LeftSide;
				} else if (nodeLayerMap[fromNode.data.key] > nodeLayerMap[toNode.data.key]) {
					// case top right column
					// if (fromNode.data.text === 'st5' && toNode.data.text === 'st7'){
					// 	console.log('===', isTopSideAvailable(fromNode, toNode), isLeftSideAvailable(toNode, fromNode))
					// }
					if (isTopSideAvailable(fromNode, toNode) && isLeftSideAvailable(toNode, fromNode)) {
						e.link.fromSpot = go.Spot.TopSide;
						e.link.toSpot = go.Spot.LeftSide;
					} else if (isRightSideAvailable(fromNode, toNode) && isBottomSideAvailable(toNode, fromNode)) {
						e.link.fromSpot = go.Spot.RightSide;
						e.link.toSpot = go.Spot.BottomSide;
					} else {
						e.link.fromSpot = go.Spot.TopSide;
						e.link.toSpot = go.Spot.BottomSide;
					}
				} else {
					// case top left column
					if (isTopSideAvailable(fromNode, toNode) && isRightSideAvailable(toNode, fromNode)) {
						e.link.fromSpot = go.Spot.TopSide;
						e.link.toSpot = go.Spot.RightSide;
					} else if (isLeftSideAvailable(fromNode, toNode) && isBottomSideAvailable(toNode, fromNode)) {
						e.link.fromSpot = go.Spot.LeftSide;
						e.link.toSpot = go.Spot.BottomSide;
					} else {
						e.link.fromSpot = go.Spot.TopSide;
						e.link.toSpot = go.Spot.BottomSide;
					}
				}
			}

			// if (
			// 	fromLanePos < toLanePos &&
			// 	nodeLayerMap[e.link.fromNode.data.key] - nodeLayerMap[e.link.toNode.data.key] === 1
			// ) {
			// 	e.link.fromSpot = go.Spot.BottomSide; // go down to next layer
			// } else if (
			// 	fromLanePos < toLanePos &&
			// 	nodeLayerMap[e.link.fromNode.data.key] > nodeLayerMap[e.link.toNode.data.key]
			// ) {
			// 	e.link.fromSpot = go.Spot.BottomSide; // go down
			// 	e.link.toSpot = go.Spot.LeftSide;
			// } else if (fromLanePos < toLanePos) {
			// 	e.link.fromSpot = go.Spot.BottomSide; // go down
			// 	e.link.toSpot = go.Spot.TopSide;
			// } else if (
			// 	fromLanePos > toLanePos &&
			// 	nodeLayerMap[e.link.fromNode.data.key] - nodeLayerMap[e.link.toNode.data.key] === 1
			// ) {
			// 	e.link.fromSpot = go.Spot.TopSide; // go up to next layer
			// 	e.link.toSpot = go.Spot.LeftSide;
			// } else if (fromLanePos > toLanePos) {
			// 	e.link.fromSpot = go.Spot.TopSide; // go up
			// 	e.link.toSpot = go.Spot.BottomSide;
			// } else {
			// 	// in the same lane
			// 	if (isSameRow(e.link.fromNode, e.link.toNode)) {
			// 		if (
			// 			nodeIndexInLaneMap[e.link.fromNode.data.key] - nodeIndexInLaneMap[e.link.toNode.data.key] ===
			// 			1
			// 		) {
			// 			e.link.fromSpot = go.Spot.RightSide;
			// 		} else {
			// 			e.link.fromSpot = go.Spot.BottomSide;
			// 			e.link.toSpot = go.Spot.BottomSide;
			// 		}
			// 	} else if (nodeLayerMap[e.link.fromNode.data.key] < nodeLayerMap[e.link.toNode.data.key]) {
			// 		// go back
			// 		if (isRowBellow(e.link.fromNode, e.link.toNode)) {
			// 			e.link.fromSpot = go.Spot.TopSide;
			// 			e.link.toSpot = go.Spot.TopSide;
			// 		} else {
			// 			e.link.fromSpot = go.Spot.BottomSide;
			// 			e.link.toSpot = go.Spot.BottomSide;
			// 		}
			// 	} else if (isRowBellow(e.link.fromNode, e.link.toNode)) {
			// 		e.link.fromSpot = go.Spot.BottomSide;
			// 	}
			// }
		}

		// turningSameSideOutLink();
	}
}
