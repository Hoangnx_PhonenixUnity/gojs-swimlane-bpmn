import React from 'react';
import './App.css';
import BPMN from './bpmn/BPMN';

import data from './data/arrow-overlap.json'

// Actual: 
// 1: The Diagram arrows are overlapping together.
// 2: Diagram phases are overlapping together, and the nodes of each phase is not grouped (by property "phase").

// More information:
// - A phase is a group of node that's grouped by property "phase".
// - The Diagram is using "a custom of Swimlane Layout" to populate nodes.

// Expect:
// 1: The Diagram arrows do not overlap.
// 2: The Diagram phases are separated. Each phase should groups nodes inside it (like the way the swimlane layout vertical groups nodes).


function App() {
  return (
    <div className="App">
      <BPMN process={data} />
    </div>
  );
}

export default App;
