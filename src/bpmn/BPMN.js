﻿
import React, { useEffect, forwardRef, useRef, useImperativeHandle } from 'react';
import go from 'gojs';

import initDiagram, { loadJSON } from './BPMNScript.ts';
import { createBPMNDiagramModel } from './BPMNHelper.js';

go.Diagram.licenseKey = process.env.REACT_APP_GOJS_LICENSE || '';

const BPMN = ({ process }, ref) => {

	useEffect(() => {
		initDiagram();

		return () => {
			delete window.myDiagram;
		};
	}, []);

	useEffect(() => {
		const initData = createBPMNDiagramModel(process);
		console.log('initData', initData)
		loadJSON(initData);
	}, [process]);

	return (
		<div ref={ref} className="h-screen w-screen">
			<div id="currentFile" className="hidden">
				(Unsaved File)
				<ul id="menuui">
					<li>
						<a href="#">File</a>
						<ul>
							<li>
								<a href="#" id="newDocument">
									New
								</a>
							</li>
							<li>
								<a href="#" id="openDocuments">
									Open...
								</a>
							</li>
							<li>
								<a href="#" id="saveDocument">
									Save
								</a>
							</li>
							<li>
								<a href="#" id="saveDocumentAs">
									Save As...
								</a>
							</li>
							<li>
								<a href="#" id="removeDocuments">
									Delete...
								</a>
							</li>
						</ul>
					</li>
					<li>
						<a href="#">Edit</a>
						<ul>
							<li>
								<a href="#" id="undo">
									Undo
								</a>
							</li>
							<li>
								<a href="#" id="redo">
									Redo
								</a>
							</li>
							<li>
								<a href="#" id="cutSelection">
									Cut
								</a>
							</li>
							<li>
								<a href="#" id="copySelection">
									Copy
								</a>
							</li>
							<li>
								<a href="#" id="pasteSelection">
									Paste
								</a>
							</li>
							<li>
								<a href="#" id="deleteSelection">
									Delete
								</a>
							</li>
							<li>
								<a href="#" id="selectAll">
									Select All
								</a>
							</li>
						</ul>
					</li>
					<li>
						<a href="#">Align</a>
						<ul>
							<li>
								<a href="#" id="alignLeft">
									Left Sides
								</a>
							</li>
							<li>
								<a href="#" id="alignRight">
									Right Sides
								</a>
							</li>
							<li>
								<a href="#" id="alignTop">
									Tops
								</a>
							</li>
							<li>
								<a href="#" id="alignBottom">
									Bottoms
								</a>
							</li>
							<li>
								<a href="#" id="alignCenterX">
									Center X
								</a>
							</li>
							<li>
								<a href="#" id="alignCenterY">
									Center Y
								</a>
							</li>
						</ul>
					</li>
					<li>
						<a href="#">Space</a>
						<ul>
							<li>
								<a href="#" id="alignRows">
									In Row...
								</a>
							</li>
							<li>
								<a href="#" id="alignColumns">
									In Column...
								</a>
							</li>
						</ul>
					</li>
					<li>
						<a href="#">Options</a>
						<ul>
							<li>
								<a href="#">
									<input id="grid" type="checkbox" name="options" value="grid" />
									Grid
								</a>
							</li>
							<li>
								<a href="#">
									<input id="snap" type="checkbox" name="options" value="0" />
									Snapping
								</a>
							</li>
						</ul>
					</li>
				</ul>
			</div>

			<div id="PaletteAndDiagram" className="relative overflow-hidden w-full h-full">
				<div id="sideBar" className="hidden float-left w-[15%] min-h-[500px] text-center">
					<span className="inline-block align-top p-1 w-full">
						<div id="accordion" className="m-0 w-[97%] min-h-625px]">
							<h4>Level 1 items</h4>
							<div>
								<div id="myPaletteLevel1" className="myPaletteDiv w-full min-h-[625px]" />
							</div>
							<h4>Level 2 items</h4>
							<div>
								<div id="myPaletteLevel2" className="myPaletteDiv w-full min-h-[625px]" />
							</div>
							<h4>Other items</h4>
							<div>
								<div id="myPaletteLevel3" className="myPaletteDiv w-full min-h-[625px]" />
							</div>
						</div>
					</span>
					<div className="handle">Overview:</div>
					<div id="myOverviewDiv" className="w-full h-[225px] bg-gray-300" />
				</div>
				<div id="myDiagramDiv" className="h-full bg-white" />
				<div id="description" className="hidden float-left ml-1">
					<ul>
						<li>
							<a href="BPMN.css">BPMN.css</a> for styling
						</li>
						<li>
							<a href="BPMNScript.ts">BPMN.ts</a> for the Diagram logic
						</li>
						<li>
							<a href="BPMNClasses.ts">BPMNClasses.ts</a> - Custom PoolLink and BPMNLinkingTool classes
						</li>
						<li>
							<a href="../../extensionsJSM/DrawCommandHandler.ts">DrawCommandHandler.ts</a> - Custom
							CommandHandler class
						</li>
					</ul>
					<span>
						<div>
							<p>Additional sample data files:</p>
							<figure>
								<img
									src="BPMNdata/BasicOrderProcess.png"
									id="basicOrderProcess"
									title="Basic Order Process"
								/>
								<figcaption>Basic Order Process</figcaption>
							</figure>
							<figure>
								<img
									src="BPMNdata/OMG BPMN by Example Figure 5.1.png"
									id="5.1"
									title="OMG BPMN by Example Figure 5.1"
								/>
								<figcaption>OMG BPMN by Example Figure 5.1</figcaption>
							</figure>
							<figure>
								<img
									src="BPMNdata/OMG BPMN by Example Figure 5.2.png"
									id="5.2"
									title="OMG BPMN by Example Figure 5.2"
								/>
								<figcaption>OMG BPMN by Example Figure 5.2</figcaption>
							</figure>
							<figure>
								<img
									src="BPMNdata/OMG BPMN by Example Figure 5.3.png"
									id="5.3"
									title="OMG BPMN by Example Figure 5.3"
								/>
								<figcaption>OMG BPMN by Example Figure 5.3</figcaption>
							</figure>
						</div>
					</span>
				</div>
			</div>

			<div
				id="openDocument"
				className="inline-block align-top bg-gray-300 absolute top-[40%] left-[50%] w-[300px] h-[200px] z-[500] hidden"
			>
				<div id="openDraggableHandle" className="handle">
					Open File
				</div>
				<div id="openText" className="elementText">
					Choose file to open...
				</div>
				<select id="mySavedFiles" className="mySavedFiles" />
				<br />
				<button id="openBtn" className="elementBtn ml-16" type="button">
					Open
				</button>
				<button id="cancelBtn" className="elementBtn" type="button">
					Cancel
				</button>
			</div>

			<div
				id="removeDocument"
				className="inline-block align-top bg-gray-300 absolute top-[40%] left-[50%] w-[300px] h-[200px] z-[500] hidden"
			>
				<div id="removeDraggableHandle" className="handle">
					Delete File
				</div>
				<div id="removeText" className="elementText">
					Choose file to remove...
				</div>
				<select id="mySavedFiles2" className="mySavedFiles" />
				<br />
				<button id="removeBtn" className="elementBtn ml-16 " type="button">
					Remove
				</button>
				<button id="cancelBtn2" className="elementBtn" type="button">
					Cancel
				</button>
			</div>

			<div className="absolute inset-0 z-10 overflow-scroll opacity-0 pointer-events-none" />
		</div>
	);
};

export default forwardRef(BPMN);
