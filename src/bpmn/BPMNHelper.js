

const nodeDataTemplate = {
	/* ----------------------------------- Level 1 ----------------------------------- */
	start: { category: 'event-default', eventType: 1, eventDimension: 1 },
	'start-event-message': { category: 'event', eventType: 2, eventDimension: 2, item: 'Message' }, // BpmnTaskMessage
	'start-event-timer': { category: 'event', eventType: 3, eventDimension: 3 },
	end: { category: 'event-default', eventType: 1, eventDimension: 8 },
	// 'end-event-none': { category: 'event-default', eventType: 1, eventDimension: 8 },
	// 'end-event-none': { category: 'event', eventType: 1, eventDimension: 8 },
	'end-event-message': { category: 'event', eventType: 2, eventDimension: 8 }, // BpmnTaskMessage
	'end-event-terminate': {
		category: 'event',
		text: 'Terminate',
		eventType: 13,
		eventDimension: 8,
	},
	// -------------------------- Task/Activity Nodes
	// task: { category: 'activity', taskType: 0 },
	process: { category: 'activity', taskType: 0 },
	'user-task': { category: 'activity', taskType: 2 },
	'service-task': { category: 'activity', taskType: 6 },
	// subprocess
	// 'subprocess-collapsed': { category: 'subprocess', loc: '0 0', isGroup: true, isSubProcess: true, taskType: 0 },
	// -------------------------- Gateway Nodes, Data, Pool and Annotation
	'gateway-parallel': { category: 'gateway', gatewayType: 1 },
	'gateway-or': { category: 'gateway', gatewayType: 4 },
	decision: { category: 'gateway', gatewayType: 4 },
	document: { category: 'dataobject' },
	// 'data-object': { category: 'dataobject' },
	'data-store': { category: 'datastore' },
	// 'participant': { category: 'privateProcess' },
	'lane-divide-two': { isGroup: 'true', category: 'Pool' },
	'text-annotation': { category: 'annotation' },
	/* --------------------------------- Level 2 -------------------------------- */
	'receive-task': { category: 'activity', taskType: 1 },
	'send-task': { category: 'activity', taskType: 5 },
	'business-rule-task': { category: 'activity', taskType: 7 },
	// 'user-task': { category: 'activity', taskType: 2, text: 'User Task', item: 'User Task', isCall: true },
	transaction: { isGroup: true, isSubProcess: true, category: 'subprocess', isAdHoc: true, taskType: 0, loc: '0 0' },
	'transaction-subprocess': {
		isGroup: true,
		isSubProcess: true,
		category: 'subprocess',
		isTransaction: true,
		taskType: 0,
		loc: '0 0',
	},
	'looping-activity': {
		isGroup: true,
		isLoop: true,
		isSubProcess: true,
		category: 'subprocess',
		taskType: 0,
		loc: '0 0',
	},
	'multi-instance-activity': {
		isGroup: true,
		isSubProcess: true,
		isParallel: true,
		category: 'subprocess',
		taskType: 0,
		loc: '0 0',
	},
	'call-subprocess': {
		isGroup: true,
		isSubProcess: true,
		category: 'subprocess',
		isCall: true,
		taskType: 0,
		loc: '0 0',
	},
	// gateway nodes
	inclusive: { category: 'gateway', gatewayType: 2 },
	'event-gateway': { category: 'gateway', gatewayType: 5 },
	// events
	'start-event-condition': { category: 'event', eventType: 5, eventDimension: 1 },
	'start-event-signal': { category: 'event', eventType: 10, eventDimension: 1 },
	'end-event-signal': { category: 'event', eventType: 10, eventDimension: 8 },
	'end-event-error': { category: 'event', eventType: 7, eventDimension: 8 },
	'end-event-escalation': { category: 'event', eventType: 4, eventDimension: 8 },
	// throwing / catching intermedicate events
	'intermediate-event-catch-link': { category: 'event', eventType: 6, eventDimension: 4 },
	'intermediate-event-throw-link': { category: 'event', eventType: 6, eventDimension: 7 },
	'intermediate-event-catch-message': { category: 'event', eventType: 2, eventDimension: 4 },
	'intermediate-event-throw-message': { category: 'event', eventType: 2, eventDimension: 7 },
	'intermediate-event-catch-condition': { category: 'event', eventType: 5, eventDimension: 4 },
	'intermediate-event-catch-timer': { category: 'event', eventType: 3, eventDimension: 4 },
	'intermediate-event-throw-escalation': { category: 'event', eventType: 4, eventDimension: 7 },
	'intermediate-event-catch-signal': { category: 'event', eventType: 10, eventDimension: 4 },
	'intermediate-event-throw-signal': { category: 'event', eventType: 10, eventDimension: 7 },
};

export const getItem = (shapeType) => {
	let item = '';
	switch (shapeType.toLowerCase()) {
		case 'start':
			item = 'start';
			break;
		case 'end':
			item = 'End';
			break;
		default:
			item = 'generic task';
			break;
	}
	return item;
};

export const createBPMNDiagramModel = (process) => {
	const { step: steps, process_id: processId, name: processName, enable_role: enableRole } = { ...process };
	const nodes = [];
	const links = [];

	// add lane node
	function addLaneNode() {
		if (enableRole) {
			let roleList = [];
			if (process.role && process.role.length > 0) {
				roleList = process.role.reduce((result, role) => {
					result[role.id] = role.name;
					return result;
				}, {});
			} else {
				steps.forEach(({ role }) => {
					if (role !== undefined && !roleList.includes(role)) roleList.push(role || '');
				});
			}

			if (roleList.length === 0) {
				nodes.push({
					text: '',
					key: 'lane_empty',
					group: 'POOL_GROUP',
					category: 'Lane',
					color: 'white',
					isGroup: true,
				});
			} else {
				roleList.forEach((role) => {
					nodes.push({
						text: role,
						key: `lane_${role || 'empty'}`,
						group: 'POOL_GROUP',
						category: 'Lane',
						color: 'white',
						isGroup: true,
					});
				});
			}
		} else {
			nodes.push({
				text: '',
				key: 'lane_empty',
				group: 'POOL_GROUP',
				category: 'Lane',
				color: 'white',
				isGroup: true,
			});
		}
	}

	const addPhaseNodes = () => {
		const phaseList = [];
		if (process.enable_phase) {
			steps.forEach(({ phase }) => {
				const p = phase ?? '';
				if (!phaseList.includes(p)) phaseList.push(p);
			});

			phaseList.forEach((phase, index) => {
				nodes.push({
					key: `phase_${phase || 'empty'}`,
					text: phase,
					category: 'PhaseGroup',
				});
			});
		}
	};

	addLaneNode();
	addPhaseNodes();

	steps.forEach(({ id, text, role, phase, width, shape_type, next_steps, decisions, roles }, index) => {
		const shapeType = shape_type.toLowerCase();

		if (shapeType === 'decision') {
			// decision nodes
			nodes.push({
				key: id,
				text,
				group: `lane_${role || 'empty'}`,
				phase: `phase_${phase || 'empty'}`,
				...nodeDataTemplate[shape_type],
				gatewayType: 4,
			});
			decisions?.forEach(({ decision_label, next_steps }) => {
				// decision links
				next_steps?.forEach(({ value }) => {
					const nextStep = steps.find(({ id }) => id === value);
					links.push({
						from: id,
						to: value,
						group: nextStep?.role,
						visible: true,
						text: decision_label,
					});
				});
			});
		} else {
			// start, end, process nodes
			nodes.push({
				key: id,
				text,
				group: `lane_${role || 'empty'}`,
				phase: `phase_${phase || 'empty'}`,
				...nodeDataTemplate[shape_type],
			});
			// start, end, process links
			next_steps?.forEach(({ value }) => {
				links.push({
					from: id,
					to: value,
					show_label: false,
				});
			});
		}
	});

	// add link to the End node
	if (links.length === 0) {
		links.push({
			from: steps[steps.length - 2].id,
			to: steps[steps.length - 1].id,
			show_label: false,
		});
	}

	return {
		class: 'go.GraphLinksModel',
		linkFromPortIdProperty: 'fromPort',
		linkToPortIdProperty: 'toPort',
		linkDataArray: links,
		nodeDataArray: nodes,
	};
};
